package com.example.user.menuestilos2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class Principal extends AppCompatActivity {
    private GridView grdOpciones;
    private Toast lblMensaje;
    public static final String [] COMIDAS = {
            "Arroz con Pollo", "Ají de Gallina", "Lomo Saltado", "Tacu Tacu", "Ceviche", "Hostia que hambre me ha dado"
    };

    public static final String [] INGREDIENTES = {
            "8 presas de pollo\n" +
                    "1 cebolla grande picada en cuadritos\n" +
                    "2 cucharadas de ajo molido o picado\n" +
                    "1 cucharada de ají amarillo molido\n" +
                    "1 cucharada de ají panca molido\n" +
                    "1 taza y ½ de culantro molido\n" +
                    "1 kg. de arroz\n" +
                    "1 taza de arvejitas\n" +
                    "1 pimiento rojo cortado en tiras\n" +
                    "Sal al gusto\n" +
                    "Pimienta al gusto\n" +
                    "Aceite\n" +
                    "Agua\n" +
                    "1 botella de cerveza negra (opcional)\n" +
                    "1 taza de maíz choclo desgranado (opcional)",
            "1 pechuga de gallina o de pollo (lo habitual es pollo)\n" +
                    "2 panes francés (ó 1 paquete de galleta de soda ó 2 rebanadas de pan de molde sin corteza)\n" +
                    "1 cebolla picada\n" +
                    "4 dientes de ajo picados\n" +
                    "3 cucharadas de ají amarillo molido (o 2 ajíes amarillos enteros)\n" +
                    "1 tarro de leche evaporada\n" +
                    "6 papas sancochadas (recomendamos papa blanca o papa amarilla)\n" +
                    "100 gramos de aceitunas negras\n" +
                    "3 huevos sancochados\n" +
                    "sal\n" +
                    "pimienta\n" +
                    "comino\n" +
                    "palillo\n" +
                    "aceite vegetal",
            "1 Kg. de carne de res (bisteck, guiso, o lomo)\n" +
                    "4 cebollas rojas medianas\n" +
                    "4 tomates\n" +
                    "1 ají amarillo\n" +
                    "2 dientes de ajo, molidos o finamente picados\n" +
                    "2 cucharaditas de sillao (salsa de soya)\n" +
                    "un chorro de vinagre tinto\n" +
                    "sal al gusto\n" +
                    "pimienta al gusto\n" +
                    "Guarnición: papas fritas, arroz blanco, perejil picado",
            "½ kilo de frejol canario\n" +
                    "½ kilo de manteca de chancho en cubitos\n" +
                    "½ kilo de arroz cocido graneado\n" +
                    "2 cdas. de aceite\n" +
                    "1 cebolla roja grande picada en cuadraditos\n" +
                    "4 dientes de ajo molidos\n" +
                    "3 cdas. de ají amarillo molido\n" +
                    "Sal, pimienta y orégano",
            "1 Kg. de pescado fresco (aunque algunos utilizan pescado congelado). En la medida de lo posible optar por pescado blanco\n" +
                    "20 limones\n" +
                    "1 cebolla roja grande o 2 medianas\n" +
                    "1 ají limo\n" +
                    "1 cucharadita de sal\n" +
                    "1 pizca de pimienta (de preferencia blanca)\n" +
                    "1 pizca de sazonador ajinomoto (opcional)\n" +
                    "Algunas hojas de culantro (opcional)\n" +
                    "Apio (opcional)\n" +
                    "Para la guarnición se puede emplear si se desea: 2 choclos cocidos, 4 camotes cocidos, 2 yucas cocidas o fritas, Plátano verde frito en rodajas (chifles), algunas hojas de lechuga",
            "..."
    };

    public static final String [] TIEMPO = {
            "45 minutos", "20 minutos", "40 minutos", "60 minutos", "10 minutos", "..."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, COMIDAS);
        grdOpciones = (GridView)findViewById(R.id.GridOpciones);
        grdOpciones.setAdapter(adaptador);
        grdOpciones.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, android.view.View v, int position, long id) {
                        lblMensaje.makeText(getApplicationContext(),"Opción seleccionada: "
                                + parent.getItemAtPosition(position),Toast.LENGTH_LONG).show();
                    }
                });
    }

}
